#include <stm32l052xx.h>
#include <snes_controller.h>

int test = 10;

int main(void)
{
	RCC->IOPENR |= RCC_IOPENR_GPIOBEN;
	GPIOB->MODER = (GPIOB->MODER & ~(0x3 << 0)) | 1;
	struct snes_controller_gpio gpios[3] = {
		{GPIOB, 5},
		{GPIOB, 3},
		{GPIOB, 4},
	};
	struct snes_controller controller;
	snes_controller_initialize(&controller, gpios);
	volatile int val = test;
	for(;;)
	{
		val++;
		if (val % 2)
			GPIOB->ODR |= 1;
		else
			GPIOB->ODR &= ~1;
		test = snes_controller_read(&controller);
	}
}
