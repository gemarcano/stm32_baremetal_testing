#ifndef SNES_CONTROLLER_H_
#define SNES_CONTROLLER_H_

#include <stm32l052xx.h>
#include <stdint.h>

struct snes_controller_gpio
{
	GPIO_TypeDef *port;
	uint8_t pin;
};

struct snes_controller
{
	struct snes_controller_gpio data, clk, latch;
};

void snes_controller_initialize(struct snes_controller *controller, const struct snes_controller_gpio *gpios);

uint16_t snes_controller_read(struct snes_controller *controller);

#endif//SNES_CONTROLLER_H_
