#include <snes_controller.h>
#include <stddef.h>

void snes_controller_set_pin(struct snes_controller_gpio *gpio, uint32_t value)
{
	gpio->port->ODR = (gpio->port->ODR & ~(1 << gpio->pin)) | (!!value << gpio->pin);
}

void snes_controller_configure_pin(struct snes_controller_gpio *gpio, uint8_t config)
{
	gpio->port->MODER = (gpio->port->MODER & ~(0x3 << (gpio->pin*2))) | (config << (gpio->pin*2));
}

void snes_controller_collector_pin(struct snes_controller_gpio *gpio, uint8_t config)
{
	gpio->port->OTYPER = (gpio->port->OTYPER & ~(1 << gpio->pin)) | (config << gpio->pin);
}


unsigned snes_controller_read_pin(struct snes_controller_gpio *gpio)
{
	return (gpio->port->IDR >> gpio->pin) & 1;
}

void snes_controller_initialize(struct snes_controller *controller, const struct snes_controller_gpio *gpios)
{
	controller->data = gpios[0];
	controller->clk = gpios[1];
	controller->latch = gpios[2];

	snes_controller_set_pin(&controller->clk, 1);
	snes_controller_set_pin(&controller->latch, 0);
	snes_controller_configure_pin(&controller->clk, 1);
	snes_controller_configure_pin(&controller->latch, 1);
	snes_controller_collector_pin(&controller->clk, 1);
	snes_controller_collector_pin(&controller->latch, 1);
	snes_controller_configure_pin(&controller->data, 0);

	// Now initialize GPIOs
	for (size_t i = 0; i < 3; ++i)
	{
		if (GPIOA == gpios[i].port)
			RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
		else if (GPIOB == gpios[i].port)
			RCC->IOPENR |= RCC_IOPENR_GPIOBEN;
		else if (GPIOC == gpios[i].port)
			RCC->IOPENR |= RCC_IOPENR_GPIOCEN;
	}
}

uint16_t snes_controller_read(struct snes_controller *controller)
{
	snes_controller_set_pin(&controller->latch, 1);
	snes_controller_set_pin(&controller->latch, 0);
	
	uint16_t result = 0;
	for (size_t i = 0; i < 16; ++i)
	{
		result |= snes_controller_read_pin(&controller->data) << i;
		snes_controller_set_pin(&controller->clk, 0);
		snes_controller_set_pin(&controller->clk, 1);
	}
	return result;
}
